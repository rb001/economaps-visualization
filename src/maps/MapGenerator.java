package maps;

import maps.Main.DataEntry;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;


/**
 * Created with IntelliJ IDEA.
 * User: rbehl425
 * Date: 1/3/14
 * Time: 12:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class MapGenerator {

    //do data analysis

     //TODO: FIX CATEGORY DESCRIPTION OF DATA ENTRY

    void dataAnalyzer(HashMap<String, DataEntry> dataSet, ArrayList<DataEntry> listOfIndicatorInfo) {

        //this holds for GDP but may have to be more discriminative later on for different indicators

        LinkedList<DataEntry> topFive = new LinkedList<DataEntry>();
        LinkedList<DataEntry> bottomFive = new LinkedList<DataEntry>();

        for (DataEntry d : listOfIndicatorInfo) {
            if (d.getDataValue() < Defaults.sevenLowerBound) {
                d.setCategory(8);
            } else if (d.getDataValue() >= Defaults.sevenLowerBound && d.getDataValue() <= Defaults.sevenUpperBound) {
                d.setCategory(7);
            } else if (d.getDataValue() >= Defaults.sixLowerBound && d.getDataValue() <= Defaults.sixUpperBound) {
                d.setCategory(6);
            } else if (d.getDataValue() >= Defaults.fiveLowerBound && d.getDataValue() <= Defaults.fiveUpperBound) {
                d.setCategory(5);
            } else if (d.getDataValue() >= Defaults.fourLowerBound && d.getDataValue() <= Defaults.fourUpperBound) {
                d.setCategory(4);
            } else if (d.getDataValue() >= Defaults.threeLowerBound && d.getDataValue() <= Defaults.threeUpperBound) {
                d.setCategory(3);
            } else if (d.getDataValue() >= Defaults.twoLowerBound && d.getDataValue() <= Defaults.twoUpperBound){
                d.setCategory(2);
            } else if (d.getDataValue() > Defaults.twoUpperBound) {
                d.setCategory(1);
            }
        }

        int length = listOfIndicatorInfo.size();
        for (int i = 0; i < 5; i += 1) {
            listOfIndicatorInfo.get(i).setIsTopFive(true);
            topFive.add(listOfIndicatorInfo.get(i));
        }

        for (int k = length - 5; k < length; k += 1) {
            listOfIndicatorInfo.get(k).setIsBottomFive(true);
            bottomFive.add(listOfIndicatorInfo.get(k));
        }

        addMarkersToMap(dataSet, listOfIndicatorInfo);
    }

    /** Write javaScript to an html file according to the above data analysis. Adds
     *  color overlays, location markers, and color legends to the corresponding map.
     * @param dataSet
     * @param listOfIndicatorInfo*/
      void addMarkersToMap(HashMap<String, DataEntry> dataSet,
                           ArrayList<DataEntry> listOfIndicatorInfo) {
          MapServlet mapservlet = new MapServlet();
          mapservlet.doGet(, dataSet, listOfIndicatorInfo);
    }

    /** A DataEntry Comparator that compares Data Entries based on their data values. */
    private class DataEntryComparator<Double> implements Comparator<DataEntry> {

        /** A comparator based on comparing doubles. */
        private Comparator<java.lang.Double> _dataValueComparator;

        /** Creates a new instance of a data entry comparator that compares Data Entries
         * based on their data values.
         * @param dataValueComparator */
        private DataEntryComparator(Comparator<java.lang.Double> dataValueComparator) {
            _dataValueComparator = dataValueComparator;
        }

        @Override
        /** Override the compare method to compare two DataEntry object's data values. */
        public int compare(DataEntry o1, DataEntry o2) {
            return _dataValueComparator.compare(o1.getDataValue(), o2.getDataValue());
        }
    }



}
