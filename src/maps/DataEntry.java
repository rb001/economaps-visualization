/**
 * 
 */
package maps;

/**
 * @author rbehl425
 *
 */
public class DataEntry {

    /** A category indicates what part of the range of values of the original data
     * set this particular data entry is located in. In this program, six is the
     * fixed number of categories. Ranges of the categories and whether they will
     * be proportional or not are not determined here but rather in MapGenerator.java
     * where most of the actual data mining and analysis takes place. E.g. Let's my range
     * of values is 1-100, and I have set my category ranges to be 1-10, 11-20, 21-30,
     * 31-40, 41-50, and 51-100. If this dataEntry has a dataValue of 25, the program
     * will assign a category number of 3 to the dataEntry. 1, 2, 3, 4, 5, and 6 are the
     * options for category assignments where 1 is the highest and 6 is the lowest in the
     * range of the data set. */
    int _category;
    /** The country of this DataEntry. */
    String _country ;
    /** The data value of this DataEntry. It could represent GDP per capita, GINI coeffient,
     * or any number of other indicators (all in float number representation). */
    Double _dataValue;
    /** True if the DataEntry is amongst the top five in the dataset. */
    boolean _isTopFive;
    /** True ifi the DataEntry is amongst the bottom five in the dataset. */
    boolean _isBottomFive;
    int _ranking;

    /** Creates a new instance of a DataEntry.
     *  @param country
     *  @param dataValue
     *  @param isTopFive
     *  @param isBottomFive */
    public DataEntry(String country, Double dataValue, boolean isTopFive, boolean isBottomFive) {
        _country = country;
        _dataValue = dataValue;
        _isTopFive = isTopFive;
        _isBottomFive = isBottomFive;
    }

    /**
     * @param string
     * @param parseDouble
     * @param b
     * @param c
     * @param parseInt
     */
    public DataEntry(String country, double dataValue, boolean isTopFive, boolean isBottomFive,
        int ranking) {
        // TODO Auto-generated constructor stub
        _country = country;
        _dataValue = dataValue;
        _isTopFive = isTopFive;
        _isBottomFive = isBottomFive;
        _ranking = ranking;
    }

    /**Sets the category of the current DataEntry.
     * @param category */
    void setCategory(int category) {
        _category = category;
    }

    /** Returns the category of the current DataEntry.
     *  @return _category */
    int getCategory() {
        return _category;
    }

    /** Returns the country of the current DataEntry.
     *  @return String */
    public String getCountry() {
        return _country;
    }

    /** Returns the data value of the current DataEntry.
     *  @return _dataValue */
    public Double getDataValue() {
        return _dataValue;
    }

    /** Sets the value of the isTopFive variable of the current DataEntry.
     * @param isTopFive */
    void setIsTopFive(boolean isTopFive) {
        _isTopFive = isTopFive;
    }

    /** Returns whether the DataEntry is amongst the top five in the data set
     * (as determined by comparing data values across all DataEntries).
     * @return _isTopFive */
    boolean getIsTopFive() {
        return _isTopFive;
    }

    /** Sets the value of the isBottomFive variable of the current DataEntry.
     * @param isBottomFive */
    void setIsBottomFive(boolean isBottomFive) {
        _isBottomFive = isBottomFive;
    }

    /** Returns whether the DataEntry is amongst the bottom five in the data set
     * (as determined by comparing data values across all DataEntries).
     * @return _isBottomFive */
    boolean getIsBottomFive() {
        return _isBottomFive;
    }
}