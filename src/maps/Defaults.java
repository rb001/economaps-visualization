package maps;

/**
 * A file of the default values to be used in analyzing the data.
 * @author Rohini Behl
 */
public class Defaults {

    /** Lower bound for the seventh division of ranges when ranking countries by GDP per capita. */
    static final double sevenLowerBound = 2000.00;
    /** Upper bound for the seventh division of ranges when ranking countries by GDP per capita. */
    static final double sevenUpperBound = 4000.99;

    /** Lower bound for the sixth division of ranges when ranking countries by GDP per capita. */
    static final double sixLowerBound = 4001.00;
    /** Upper bound for the sixth division of ranges when ranking countries by GDP per capita. */
    static final double sixUpperBound = 12000.99;

    /** Lower bound for the fifth division of ranges when ranking countries by GDP per capita. */
    static final double fiveLowerBound = 12001.00;
    /** Upper bound for the fifth division of ranges when ranking countries by GDP per capita. */
    static final double fiveUpperBound = 18000.99;

    /** Lower bound for the fourth division of ranges when ranking countries by GDP per capita. */
    static final double fourLowerBound = 18001.00;
    /** Upper bound for the fourth division of ranges when ranking countries by GDP per capita. */
    static final double fourUpperBound = 25000.99;

    /** Lower bound for the third division of ranges when ranking countries by GDP per capita. */
    static final double threeLowerBound = 25001.00;
    /** Upper bound for the third division of ranges when ranking countries by GDP per capita. */
    static final double threeUpperBound = 40000.99;

    /** Lower bound for the second division of ranges when ranking countries by GDP per capita. */
    static final double twoLowerBound = 40001.00;
    /** Upper bound for the second division of ranges when ranking countries by GDP per capita. */
    static final double twoUpperBound = 55000.99;

}
