package maps;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;



/**Conducts data analysis on the downloaded data set, divides the countries into categories,
 * and prepares the data so the program is ready to output javascript to generate the corresponding
 * world map.
 * @author Rohini Behl
 */
public class DataMiner {

    public static ArrayList<DataEntry> wealthiestCountries = new ArrayList<DataEntry>();
    public static ArrayList<DataEntry> poorestCountries = new ArrayList<DataEntry>();
    public static ArrayList<DataEntry> listOfDataEntries = new ArrayList<DataEntry>();

    /** Sorts the countries into categories based on rankings of their GDP. */
    public void dataAnalyzer(HashMap<String, DataEntry> dataSet, ArrayList<DataEntry> listOfIndicatorInfo) {

        ArrayList<DataEntry> topFive = new ArrayList<DataEntry>();
        ArrayList<DataEntry> bottomFive = new ArrayList<DataEntry>();

        for (DataEntry d : listOfIndicatorInfo) {
            if (d.getDataValue() < Defaults.sevenLowerBound) {
                d.setCategory(8);
            } else if (d.getDataValue() >= Defaults.sevenLowerBound && d.getDataValue() <= Defaults.sevenUpperBound) {
                d.setCategory(7);
            } else if (d.getDataValue() >= Defaults.sixLowerBound && d.getDataValue() <= Defaults.sixUpperBound) {
                d.setCategory(6);
            } else if (d.getDataValue() >= Defaults.fiveLowerBound && d.getDataValue() <= Defaults.fiveUpperBound) {
                d.setCategory(5);
            } else if (d.getDataValue() >= Defaults.fourLowerBound && d.getDataValue() <= Defaults.fourUpperBound) {
                d.setCategory(4);
            } else if (d.getDataValue() >= Defaults.threeLowerBound && d.getDataValue() <= Defaults.threeUpperBound) {
                d.setCategory(3);
            } else if (d.getDataValue() >= Defaults.twoLowerBound && d.getDataValue() <= Defaults.twoUpperBound){
                d.setCategory(2);
            } else if (d.getDataValue() > Defaults.twoUpperBound) {
                d.setCategory(1);
            }
        }

        int length = listOfIndicatorInfo.size();
        for (int i = 0; i < 5; i += 1) {
            listOfIndicatorInfo.get(i).setIsTopFive(true);
            topFive.add(listOfIndicatorInfo.get(i));
        }

        for (int k = length - 5; k < length; k += 1) {
            listOfIndicatorInfo.get(k).setIsBottomFive(true);
            bottomFive.add(listOfIndicatorInfo.get(k));
        }
        listOfDataEntries = listOfIndicatorInfo;
       
        wealthiestCountries = topFive;
  
        poorestCountries = bottomFive;
//        addMarkersToMap();
    }

    /** Write javaScript to an html file according to the above data analysis. Adds
     *  color overlays, location markers, and color legends to the corresponding map.
     * @param dataSet
     * @param listOfIndicatorInfo
     * @param topFive
     * @param bottomFive*/
//    void addMarkersToMap() {
//        org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger("DataMiner.class");
//        try {
//            HttpURLConnection sr;
//            URL address = new URL("http://localhost:8080/Economapsvs2/servlet/maps.MapServlet");
//            sr = (HttpURLConnection) address.openConnection();
//            sr.setRequestMethod("POST");
//            sr.setDoOutput(true);
//            sr.setRequestProperty("User-Agent", "Chrome/32.0.1700.77 (compatible)");
//            sr.setRequestProperty("Accept", "*/*");
//
//            InputStream is = null;
//            try {
//                log.error("Trying to get the input stream..");
//                is =sr.getInputStream();
//            } catch(Exception e) {
//                e.printStackTrace();
//                is = sr.getErrorStream();
//            }
//            BufferedReader in = new BufferedReader(new InputStreamReader(is));
//
//            String inputLine;
//            int i =0;
//            while ((inputLine = in.readLine()) != null) {
//                i = i +1;
//                System.out.println(inputLine);
//            }
//            in.close();
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }

    /** A DataEntry Comparator that compares Data Entries based on their data values. */
    private class DataEntryComparator<Double> implements Comparator<DataEntry> {

        /** A comparator based on comparing doubles. */
        private Comparator<java.lang.Double> _dataValueComparator;

        /** Creates a new instance of a data entry comparator that compares Data Entries
         * based on their data values.
         * @param dataValueComparator */
        private DataEntryComparator(Comparator<java.lang.Double> dataValueComparator) {
            _dataValueComparator = dataValueComparator;
        }

        @Override
        /** Override the compare method to compare two DataEntry object's data values. */
        public int compare(DataEntry o1, DataEntry o2) {
            return _dataValueComparator.compare(o1.getDataValue(), o2.getDataValue());
        }
    }
}
