package maps;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/** Downloads the economic indicator data from the World Bank API and saves it in .csv format
 *  to be analyzed.
 *  @author Rohini Behl
 */
public class Main {

    /** Initializes the Economaps program. */
    public static void main(String[] args) {
        dataParser("src/maps/GDP.csv");
    }

    //    /** Retrieves the coordinates of all countries and sets the latitudes and longitudes of those
    //     * countries in their corresponding Data Entry objects.
    //     * @param countryCoordinatesFile
    //     * @param countryDataMappings
    //     * @param dataEntries */
    //    private static void retrieveCoordinatesOfCountries(String countryCoordinatesFile,
    //        HashMap<String, DataEntry> countryDataMappings,
    //        ArrayList<DataEntry> dataEntries) {
    //        BufferedReader reader = null;
    //        String line = "";
    //        String splitBy = ",";
    //
    //        try {
    //            reader = new BufferedReader(new FileReader(countryCoordinatesFile));
    //        } catch (FileNotFoundException e) {
    //            e.printStackTrace();
    //        }
    //
    //        try {
    //            while ((line = reader.readLine()) != null) {
    //                line.trim();
    //                String[] countries = line.split(splitBy) ;
    //                if (countries[0].equals("Country")) {
    //                    continue;
    //                }
    //                if ((countries[0]).equals("")) {
    //                    continue;
    //                }
    //                DataEntry currentDataEntry = countryDataMappings.get(countries[0]);
    //                currentDataEntry.setLatitude(Double.parseDouble(countries[1]));
    //                currentDataEntry.setLongitude(Double.parseDouble(countries[2]));
    //            }
    //        } catch (IOException e) {
    //            e.printStackTrace();
    //        } finally {
    //            if (reader != null) {
    //                try {
    //                    reader.close();
    //                } catch (IOException e) {
    //                    e.printStackTrace();
    //                }
    //            }
    //        }
    //    }

    /** Parses the csv file of country information and stores the country name, its ranking,
     *  and data value in a Data Entry object.
     * @param csvFileToRead  */
    static void dataParser(String csvFileToRead) {
        HashMap<String, DataEntry> TwoThousandTwelve = new HashMap<String, DataEntry>();
        ArrayList<DataEntry> listOfDataEntries = new ArrayList<DataEntry>();

        //reading and parsing the csv file
        //String csvFileToRead = "Downloads/csvToRead.csv"; //check this
        BufferedReader br = null;
        String line = "";
        String splitBy = ",";

        try {
            File file = new File(csvFileToRead);
            System.out.println(new File(".").getAbsoluteFile());
            System.out.println(file.exists());
            br = new BufferedReader(new FileReader(file));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            while ((line = br.readLine()) != null) {
                line.trim();
                String[] countries = line.split(splitBy) ;
                for (String s : countries) {
                    s = s.replace("#", "");
                }
                System.out.println(line);
                if (countries.length != 4) {
                    continue;
                }
                if ((countries[0]).equals("")) {
                    continue;
                }
                if(!(countries[1].matches(".*\\d.*"))){
                    continue;
                }
                System.out.println("Reading data from CSV:");
                System.out.println("Country code:" + countries[0] + " , Ranking:"
                    + countries[1] + " , Country Name:" + countries[2]
                        + " , Indicator Value (millions of US dollars:"
                        + countries[3]);
                DataEntry entry = new DataEntry(countries[2], Double.parseDouble(countries[3]),
                    false, false, Integer.parseInt(countries[1]));
                TwoThousandTwelve.put(countries[0], entry);
                listOfDataEntries.add(entry);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Done reading and storing data in CSV.");
        //        retrieveCoordinatesOfCountries("src/maps/CountryList_LatitudeAndLongitude.csv", TwoThousandTwelve, listOfDataEntries);
        DataMiner miner = new DataMiner();
        miner.dataAnalyzer(TwoThousandTwelve, listOfDataEntries);
    }


    /** A data entry consists of category, country, dataValue, isBottomFive, and isTopFive
     * variables. Its variables will be used in later sorting and data mining of data set
     * as well as mapping onto the world map provided by the Google Maps API. */
    protected static class DataEntry {

        /** A category indicates what part of the range of values of the original data
         * set this particular data entry is located in. In this program, six is the
         * fixed number of categories. Ranges of the categories and whether they will
         * be proportional or not are not determined here but rather in MapGenerator.java
         * where most of the actual data mining and analysis takes place. E.g. Let's my range
         * of values is 1-100, and I have set my category ranges to be 1-10, 11-20, 21-30,
         * 31-40, 41-50, and 51-60, 61-70, and 71-100. If this dataEntry has a dataValue of 25, the program
         * will assign a category number of 3 to the dataEntry. 1, 2, 3, 4, 5, 6, 6, and 8 are the
         * options for category assignments where 1 is the highest and 8 is the lowest in the
         * range of the data set. */
        int _category;
        /** The country of this DataEntry. */
        String _country ;
        /** The data value of this DataEntry. It could represent GDP per capita, GINI coeffient,
         * or any number of other indicators (all in float number representation). */
        Double _dataValue;
        /** True if the DataEntry is amongst the top five in the dataset. */
        boolean _isTopFive;
        /** True ifi the DataEntry is amongst the bottom five in the dataset. */
        boolean _isBottomFive;
        /** Place in flat ranking of all the countries by their corresponding data values. */
        int _ranking;
        /** Latitude of center of country corresponding to this data entry. */
        double _latitude;
        /** Longitude of center of country corresponding to this data entry. */
        double _longitude;

        /** Creates a new instance of a DataEntry.
         *  @param country
         *  @param dataValue
         *  @param isTopFive
         *  @param isBottomFive
         *  @param ranking */
        public DataEntry(String country, Double dataValue, boolean isTopFive, boolean isBottomFive, int ranking) {
            _country = country;
            _dataValue = dataValue;
            _ranking = ranking;
            _isTopFive = isTopFive;
            _isBottomFive = isBottomFive;
        }

        /**Sets the category of the current DataEntry.
         * @param category */
        void setCategory(int category) {
            _category = category;
        }

        /** Returns the category of the current DataEntry.
         *  @return _category */
        int getCategory() {
            return _category;
        }

        /** Returns the country of the current DataEntry.
         *  @return String */
        String getCountry() {
            return _country;
        }

        /** Returns the data value of the current DataEntry.
         *  @return _dataValue */
        Double getDataValue() {
            return _dataValue;
        }

        /** Sets the value of the isTopFive variable of the current DataEntry.
         * @param isTopFive */
        void setIsTopFive(boolean isTopFive) {
            _isTopFive = isTopFive;
        }

        /** Returns whether the DataEntry is amongst the top five in the data set
         * (as determined by comparing data values across all DataEntries).
         * @return _isTopFive */
        boolean getIsTopFive() {
            return _isTopFive;
        }

        /** Sets the value of the isBottomFive variable of the current DataEntry.
         * @param isBottomFive */
        void setIsBottomFive(boolean isBottomFive) {
            _isBottomFive = isBottomFive;
        }

        /** Returns whether the DataEntry is amongst the bottom five in the data set
         * (as determined by comparing data values across all DataEntries).
         * @return _isBottomFive */
        boolean getIsBottomFive() {
            return _isBottomFive;
        }

        /** Sets the ranking of the current Data Entry (by looking at its data values)
         * @param ranking */
        void setRanking(int ranking) {
            _ranking = ranking;
        }

        /** Returns the flat ranking of this Data Entry in relation to all other Data
         * Entries in this data set.
         * @return _ranking */
        int getRanking() {
            return _ranking;
        }

        /** Sets the x-coordinate of the center of the country associated with this Data Entry.
         * @param latitude */
        void setLatitude(double latitude) {
            _latitude = latitude;
        }

        /** Returns the x-coordinate of the center of the country associated with this Data Entry.
         * @return _latitude */
        double getLatitude() {
            return _latitude;
        }

        /** Sets the y-coordinate of the center of the country associated with this Data Entry.
         * @param longitude */
        void setLongitude(double longitude) {
            _longitude = longitude;
        }

        /** Returns the y-coordinate of the center of the country associated with this Data Entry.
         * @return _longitude */
        double getLongitude() {
            return _longitude;
        }
    }
}

