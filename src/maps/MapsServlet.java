package maps;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import maps.Main.DataEntry;


/**
 * Servlet implementation class ProjectServlet
 */
@WebServlet("/ProjectServlet") class MapServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MapServlet() {
        super();
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        // Set response content type

        ArrayList<DataEntry> listOfDataEntries = getDataEntriesList();
        ArrayList<DataEntry> poorestCountries = getPoorestCountriesList();
        ArrayList<DataEntry> wealthiestCountries = getWealthiestCountriesList();
        response.setContentType("text/html");

        // Actual logic goes here.
        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title> Economaps Visualization </title>");
        out.println("<script type='text/javascript' src='https://www.google.com/jsapi'></script>");
        out.println("<script type='text/javascript'>");
        out.println("google.load('visualization', '1', {'packages': ['geomap']});");
        out.println("google.setOnLoadCallback(drawMap);");
        out.println("function drawMap() {");
        out.println("var data = google.visualization.arrayToDataTable([");
        out.println("['Country', 'GDP per capita'],");
        for (DataEntry entry : listOfDataEntries) {
            out.println("[\"" + entry.getCountry() + "\", \"" + entry.getDataValue() + "\"],\"");
        }
        out.println("]);");

        out.println("var options = {};");
        out.println("options['dataMode'] = 'regions';");

        out.println("var container = document.getElementById('map_canvas');");
        out.println("var geomap = new google.visualization.GeoMap(container);");
        out.println("geomap.draw(data, options);");
        out.println(")};");
        out.println("</script>");

        out.println("<body>");
        out.println("<div id=\"map_canvas\" style=\"width:1000px;height:650px;\"></div>");
        out.println("</body>");
        out.println("</head>");

        out.println("<div style=\"position: absolute; top: 40px; right:55px; height:75px\">");
        out.println("<table border=\"1\">");
        out.println("<caption> <font face = \"Bookman Old Style\", size = \"4\", color = \"#FFFFFF\">WEALTHIEST</font></caption>");
        out.println("<colgroup>");
        out.println("<td width=\"100px\"> <i> <font color = \"#FFFFFF\"> Country </font> </i></td>");
        out.println("<td width=\"100px\"> <i> <font color = \"#FFFFFF\"> GDP per capita (USD)</font> </i></td>");
        out.println("</colgroup>");
        out.println("</table>");

        out.println("<table border=\"1\">");
        out.println("<colgroup>");
        out.println("<td width=\"100px\"></td>");
        out.println("<td width=\"100px\"></td>");
        out.println("</colgroup>");
        out.println("<tbody>");
        out.println("<tr>");
        out.println("<td><font color = \"#FFFFFF\">\"" + wealthiestCountries.get(0).getCountry() + "\"</font></td>");
        out.println("<td><font color = \"#FFFFFF\">\"" + wealthiestCountries.get(0).getDataValue() + "\"</font></td>");
        out.println("</tr>");
        out.println("<tr>");
        out.println("<td><font color = \"#FFFFFF\">\"" + wealthiestCountries.get(1).getCountry() + "\"</font></td>");
        out.println("<td><font color = \"#FFFFFF\">\"" + wealthiestCountries.get(1).getDataValue() + "\"</font></td>");
        out.println("</tr>");
        out.println("<tr>");
        out.println("<td><font color = \"#FFFFFF\">\"" + wealthiestCountries.get(2).getCountry() + "\"</font></td>");
        out.println("<td><font color = \"#FFFFFF\">\"" + wealthiestCountries.get(2).getDataValue() + "\"</font></td>");
        out.println("</tr>");
        out.println("<tr>");
        out.println("<td><font color = \"#FFFFFF\">\"" + wealthiestCountries.get(3).getCountry() + "\"</font></td>");
        out.println("<td><font color = \"#FFFFFF\">\"" + wealthiestCountries.get(3).getDataValue() + "\"</font></td>");
        out.println("</tr>");
        out.println("<tr>");
        out.println("<td><font color = \"#FFFFFF\">\"" + wealthiestCountries.get(4).getCountry() + "\"</font></td>");
        out.println("<td><font color = \"#FFFFFF\">\"" + wealthiestCountries.get(4).getDataValue() + "\"</font></td>");
        out.println("</tr>");
        out.println("</tbody>");
        out.println("</table>");
        out.println("</div>");

        out.println("<div style = \"position: absolute; top: 320px; right:55px; height:75px\">");
        out.println("<table border=\"1\">");
        out.println("<caption> <font face = \"Bookman Old Style\", size = \"4\", color = \"#FFFFFF\">MOST IMPOVERISHED</font></caption>");
        out.println("<colgroup>");
        out.println("<td width=\"100px\"> <i> <font color = \"#FFFFFF\"> Country </font> </i></td>");
        out.println("<td width=\"100px\"> <i> <font color = \"#FFFFFF\"> GDP per capita (USD)</font> </i></td>");
        out.println("</colgroup>");
        out.println("</table>");

        out.println("<table border=\"1\">");
        out.println("<colgroup>");
        out.println("<td width=\"100px\"></td>");
        out.println("<td width=\"100px\"></td>");
        out.println("</colgroup>");
        out.println("<tbody>");
        out.println("<tr>");
        out.println("<td><font color = \"#FFFFFF\">\"" + poorestCountries.get(0).getCountry() + "\"</font></td>");
        out.println("<td><font color = \"#FFFFFF\">\"" + poorestCountries.get(0).getDataValue() + "\"</font></td>");
        out.println("</tr>");
        out.println("<tr>");
        out.println("<td><font color = \"#FFFFFF\">\"" + poorestCountries.get(1).getCountry() + "\"</font></td>");
        out.println("<td><font color = \"#FFFFFF\">\"" + poorestCountries.get(1).getDataValue() + "\"</font></td>");
        out.println("</tr>");
        out.println("<tr>");
        out.println("<td><font color = \"#FFFFFF\">\"" + poorestCountries.get(2).getCountry() + "\"</font></td>");
        out.println("<td><font color = \"#FFFFFF\">\"" + poorestCountries.get(2).getDataValue() + "\"</font></td>");
        out.println("</tr>");
        out.println("<tr>");
        out.println("<td><font color = \"#FFFFFF\">\"" + poorestCountries.get(3).getCountry() + "\"</font></td>");
        out.println("<td><font color = \"#FFFFFF\">\"" + poorestCountries.get(3).getDataValue() + "\"</font></td>");
        out.println("</tr>");
        out.println("<tr>");
        out.println("<td><font color = \"#FFFFFF\">\"" + poorestCountries.get(4).getCountry() + "\"</font></td>");
        out.println("<td><font color = \"#FFFFFF\">\"" + poorestCountries.get(4).getDataValue() + "\"</font></td>");
        out.println("</tr>");
        out.println("</tbody>");
        out.println("</table>");
        out.println("</div>");
        out.println("</body>");

        out.println("</html>");
    }


    /**
     * @return
     */
    private ArrayList<DataEntry> getWealthiestCountriesList() {
        // TODO Auto-generated method stub
        return DataMiner.wealthiestCountries;
    }

    /**
     * @return
     */
    private ArrayList<DataEntry> getPoorestCountriesList() {
        // TODO Auto-generated method stub
        return DataMiner.poorestCountries;
    }

    /**
     * @return
     */
    private ArrayList<DataEntry> getDataEntriesList() {
        return DataMiner.listOfDataEntries;
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doPost(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

}