package lkj;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import maps.DataEntry;
import maps.DataMiner;

import org.json.JSONException;
import org.json.JSONObject;


public class MapsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    DataMiner miner = new DataMiner();


    public void initialize(String filename) {
        HashMap<String, DataEntry> TwoThousandTwelve = new HashMap<String, DataEntry>();
        ArrayList<DataEntry> listOfDataEntries = new ArrayList<DataEntry>();

        //reading and parsing the csv file
        //String csvFileToRead = "Downloads/csvToRead.csv"; //check this
        BufferedReader br = null;
        String line = "";
        String splitBy = ",";

        try {
            File file = new File("../GDP.csv");
//            System.out.println((file).getCanonicalPath());
            System.out.println(new File(".").getCanonicalPath());
            System.out.println(file.exists());
            System.out.println(file.canRead());
            br = new BufferedReader(new FileReader(file));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try {
            while ((line = br.readLine()) != null) {
                line.trim();
                String[] countries = line.split(splitBy) ;
                for (String s : countries) {
                    s = s.replace("#", "");
                }
                System.out.println(line);
                if (countries.length != 4) {
                    continue;
                }
                if ((countries[0]).equals("")) {
                    continue;
                }
                if(!(countries[1].matches(".*\\d.*"))){
                    continue;
                }
                System.out.println("Reading data from CSV:");
                System.out.println("Country code:" + countries[0] + " , Ranking:"
                    + countries[1] + " , Country Name:" + countries[2]
                        + " , Indicator Value (millions of US dollars:"
                        + countries[3]);
                DataEntry entry = new DataEntry(countries[2], Double.parseDouble(countries[3]),
                    false, false, Integer.parseInt(countries[1]));
                TwoThousandTwelve.put(countries[0], entry);
                listOfDataEntries.add(entry);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Done reading and storing data in CSV.");
        //        retrieveCoordinatesOfCountries("src/maps/CountryList_LatitudeAndLongitude.csv", TwoThousandTwelve, listOfDataEntries);

        miner.dataAnalyzer(TwoThousandTwelve, listOfDataEntries);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        // Set response content type

        //        System.out.println("inside servlet doget");
//        System.out.println("request" + request);
        String indicator = request.getParameter("indicator");
        if (indicator.equals("gdp")) {
            initialize("src/GDP.csv");
        }
        ArrayList<DataEntry> listOfDataEntries = getDataEntriesList();
        System.out.println("data entries:" + listOfDataEntries);
        ArrayList<DataEntry> poorestCountries = getPoorestCountriesList();
        System.out.println(poorestCountries);
        ArrayList<DataEntry> wealthiestCountries = getWealthiestCountriesList();
        System.out.println(wealthiestCountries);

        JSONObject json = new JSONObject();
        try {
            json.put("dataEntries", listOfDataEntries);
            json.put("bottomFive", poorestCountries);
            json.put("topFive", wealthiestCountries);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        System.out.println(json);
        request.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter writer = response.getWriter();
        writer.write(json.toString());


    }


    /**
     * @return
     */
    private ArrayList<DataEntry> getWealthiestCountriesList() {
        // TODO Auto-generated method stub
        return miner.wealthiestCountries;
    }

    /**
     * @return
     */
    private ArrayList<DataEntry> getPoorestCountriesList() {
        // TODO Auto-generated method stub
        return miner.poorestCountries;
    }

    /**
     * @return
     */
    private ArrayList<DataEntry> getDataEntriesList() {
        return miner.listOfDataEntries;
    }



    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doPost(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

}