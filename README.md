# README #

Info
-----
This is a web app developed in Eclipse J2EE with Tomcat version 7 used as a server. I utilized jQuery, JavaScript, and HTML as well as the World Bank and Google Maps APIs. Heroku hosts this app for free and a demo can be found at http://rocky-escarpment-5767.herokuapp.com/.

Setup
------
Navigate to the directory where you want this directory to be cloned into.

Run the following command in the terminal.

git clone https://rb001@bitbucket.org/rb001/economaps-visualization.git